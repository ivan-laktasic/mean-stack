
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongo = require('mongodb').MongoClient;
var bcrypt = require('bcrypt-nodejs');
var path = require('path');
var config = require('./dbConfig');
var jwt = require('jsonwebtoken');
var multer=require('multer');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(__dirname+'/public'));

var upload = multer({dest: './public/assets/img/upload/'}).single('file');
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, \ Authorization');
    next();
});

app.use(morgan('dev'));

mongo.connect(config.db, function(err, database){
    var authRouter = require('./app/routes/authenticate')(app, express, database, bcrypt,jwt,config.secret);
    app.use('/authenticate', authRouter);

    var apiRouter = require('./app/routes/api')(app, express, database, bcrypt,jwt,config.secret,upload);
    app.use('/api', apiRouter);

    var noTokenApiRouter = require('./app/routes/noTokenApi')(app, express, database);
    app.use('/noTokenApi', noTokenApiRouter);

    var registerRouter = require('./app/routes/register')(app, express, database, bcrypt,jwt,config.secret);
    app.use('/register', registerRouter);

    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
    });
    app.listen(config.port);
    console.log('Running on port ' + config.port);
});
