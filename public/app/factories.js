bikeApp.factory('ProductFactory',function($http){

    return {
        getProduct:function(callback){
            $http.get('/noTokenApi/products',{cache:true}).success(callback);
        },
        postProduct:function(product, callback){
            $http.post('/api/products',{product: product}).success(callback);
        },
        deleteProduct:function(id, callback){
            $http.delete('/api/products/'+id).success(callback);
        },
        editProduct:function(product, callback){
            $http.put('/api/products',{product: product}).success(callback);
        }
    }
});

bikeApp.factory('UserFactory',function($http){

    return {
        getUsers:function(callback){
            $http.get('/api/users',{cache:true}).success(callback);
        },
        postUser:function(user, callback){
            $http.post('/register/users',{user: user}).success(callback);
        },
        deleteUser:function(id, callback){
            $http.delete('/api/users/'+id).success(callback);
        },
        editUser:function(user,passChange, callback){
            $http.put('/api/users',{user: user,passChange:passChange}).success(callback);
        },
        getUser:function(id,callback){
            $http.get('/api/users/'+id).success(callback);
        }
    }
});

bikeApp.factory('OrderFactory',function($http){

    return {
        getOrder:function(callback){
            $http.get('/api/orders',{cache:true}).success(callback);
        },
        postOrder:function(order, callback){
            $http.post('/api/orders',{order: order}).success(callback);
        },
        deleteOrder:function(id, callback){
            $http.delete('/api/orders/'+id).success(callback);
        },
        editOrder:function(order, callback){
            $http.put('/api/orders',{order: order}).success(callback);
        },
        getUserOrders:function(id,callback){
            $http.get('/api/orders/'+id).success(callback);
        }
    }
});

bikeApp.factory('CategoryFactory',function($http){

    return {
        getCategories:function(callback){
            $http.get('/noTokenApi/categories',{cache:true}).success(callback);
        },
        postCategory:function(category, callback){
            $http.post('/api/categories',{category: category}).success(callback);
        },
        deleteCategory:function(id, callback){
            $http.delete('/api/categories/'+id).success(callback);
        },
        editCategory:function(category, callback){
            console.log(category)
            $http.put('/api/categories',{category: category}).success(callback);
        },
        getCategory:function(id,callback){
            console.log(id);
            $http.get('/noTokenApi/categories/'+id).success(callback);
        }

    }
});

bikeApp.factory('AuthToken', function($window){
    return {
        getToken : function(){
            return $window.localStorage.getItem('token');
        },
        setToken : function(token) {
            if (token) {
                $window.localStorage.setItem('token', token);
            } else {
                $window.localStorage.removeItem('token');
            }
        }
    }
});

bikeApp.factory('Auth', function($http, $q, AuthToken, $rootScope,$location){
    return {
        login : function(credentials, callback){
            $http.post('/authenticate', {credentials: credentials}).success(function(data) {
                console.log(data);
                if (data.user != undefined) {
                    AuthToken.setToken(data.user.token);
                    $rootScope.user = data.user;
                    $rootScope.authenticated = true;
                    $location.path('/')
                    callback(data.user);
                } else {
                    callback(data);
                }
            });
        },
        logout : function(){
            $rootScope.authenticated=false;
            $rootScope.user={};
            AuthToken.setToken();
        },
        isLoggedIn:function(){
            return  (AuthToken.getToken()) ? true : false;
        },
        register : function(credentials){
                $http.post('/register/users', {user: credentials}).success(function (data) {
                    if (data.status == 'ok') {
                    } else {
                        alert('Error while registering');
                    }
                });
        }
    };
});

bikeApp.factory('interceptorService', function($location, $q, AuthToken, $rootScope){
    var interceptorService = {};
    interceptorService.request=function (config){
        var token=AuthToken.getToken();
        if (token) config.headers['x-access-token'] = token;
        return config;
    };
    interceptorService.responseError=function(response){
        if (response.status == 403){
            AuthToken.setToken();
            $rootScope.redirected=true;
        }
        return $q.reject(response);
    };
    return interceptorService;
});

bikeApp.factory('multipartForm',['$http',function($http){

    this.post=function(data){
        console.log(data);
        var formData= new FormData();
        for(var key in data){
            formData.append(key,data[key]);
        }
        console.log(formData);
        $http.post('/api/upload',formData,{
            transformRequest:angular.identity,
            headers:{'Content-Type':undefined}
        })
    }
}])