
var bikeApp  = angular.module('bikeApp', ['ngRoute']).run(function($rootScope, Auth, $http,$location){
    $rootScope.authenticated=Auth.isLoggedIn() ? true : false;
    if ($rootScope.authenticated){
        $http.get('api/me').success(function(data){
            $rootScope.user=data;
        });
    } else {

        $rootScope.user={}

    }
    $rootScope.$on('$routeChangeStart', function(event, nextRoute) {
        if (nextRoute.controller === 'AdminCtrl') {
            if ($rootScope.user && $rootScope.user.level!=1) {
                alert("You do not have permission to view that page");
                $location.path('#/');
                return event.preventDefault();
            }
        }
    });
});

bikeApp.config(function($routeProvider,$httpProvider) {
    $routeProvider.
    when('/', {
        templateUrl: 'app/views/shop.html',
        controller: 'HomeCtrl'
    }).when('/admin',{
        templateUrl: 'app/views/admin.html',
        controller: 'AdminCtrl'
    }).when('/myAcc',{
        templateUrl: 'app/views/myAcc.html',
        controller: 'MyAccCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });
    $httpProvider.interceptors.push('interceptorService');

});