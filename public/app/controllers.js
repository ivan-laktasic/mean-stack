
bikeApp.controller('AdminCtrl',function($scope,Auth,$rootScope,UserFactory,CategoryFactory, ProductFactory,$location,multipartForm,OrderFactory){


    if ($rootScope.authenticated){
        UserFactory.getUser($rootScope.user._id,function(data){
            $scope.user=data.users[0];
        });
    }else{
        $location .path('#/');
    }
    $scope.selected='proizvodi';

    CategoryFactory.getCategories(function(data){
        $scope.categories=data.categories;
    })

    UserFactory.getUsers(function(data){
        $scope.users=data.users;
    })

    $scope.deleteUser = function(index){

        UserFactory.deleteUser($scope.users[index]._id, function(data){
            console.log(data);
            $scope.users.splice(index,1);
        });
    };

    OrderFactory.getOrder(function (data) {
        $scope.orders=data.orders;
    })

    $scope.deleteOrder=function(index){
        OrderFactory.deleteOrder($scope.order[index]._id,function(data){
            $scope.orders.splice(1,index);
        });
    }

    $scope.adminSwitch=function(index){
        if($scope.users[index].level==1){
            $scope.users[index].level=2;
            UserFactory.editUser($scope.users[index],false,function(data){

            })
        }else{
            $scope.users[index].level=1;
            UserFactory.editUser($scope.users[index],false,function(data){

            })
        }
    }

    $scope.edit=-1;

    ProductFactory.getProduct(function (data) {
        $scope.products=data.products;
    })

    $scope.modalShow=function(modal){
        if($scope.displayedModal==modal){
            $scope.displayedModal=1
        }
        else {
            $scope.displayedModal=modal;
        }
    }

    $scope.addProduct=function(){

        multipartForm.post($scope.newProduct,function(data){
            console.log(data);
            console.log($scope.newProduct.category);
            $scope.addCatt={};
            var  product= {
                name:$scope.newProduct.name,
                description:$scope.newProduct.description,
                price:$scope.newProduct.price,
                onStock:$scope.newProduct.onStock,
                categoryName:'',
                categoryId:'',
                 picUrl:'../../assets/img/upload/'+data.url
            }
            CategoryFactory.getCategory($scope.newProduct.category,function(data){
                $scope.addCatt=data.categories[0];
                console.log( $scope.addCatt);
                product.categoryName= $scope.addCatt.name;
                product.categoryId= $scope.addCatt._id;
                ProductFactory.postProduct(product,function (data) {
                    $scope.products.push({
                        _id: data.insertId,
                        name: product.name,
                        description: product.description,
                        price: product.price,
                        categoryName: product.categoryName,
                        categoryId: product.categoryId,
                        onStock: product.onStock,
                         picUrl:product.picUrl
                    })

                });
            });
        });


    }

    $scope.editProduct=function(i){
        console.log(i);
        $scope.edit=i;
    };

    $scope.doneEditingProduct=function(index){
        $scope.edit=-1;
        console.log($scope.products[index].category);
        CategoryFactory.getCategory($scope.products[index].category,function(data){
            $scope.products[index].categoryName=data.categories[0].name;
            $scope.products[index].categoryId=data.categories[0]._id;

        });
        console.log($scope.products[index]);
        if($scope.products[index].file!=undefined){
            multipartForm.post($scope.products[index],function(data) {
                $scope.products[index].picUrl='../../assets/img/upload/'+data.url;
                ProductFactory.editProduct($scope.products[index], function (data) {
                });
             });
        }else{
            ProductFactory.editProduct($scope.products[index], function (data) {
            });
        }
    };

    $scope.deleteProduct = function(index){

    ProductFactory.deleteProduct($scope.products[index]._id, function(data){
            console.log(data);
            $scope.products.splice(index,1);
        });
    };

    $scope.logout=function(){
        Auth.logout();
        $scope.user=undefined;
        $scope.authenticated=false;
        $scope.displayedModal=1;
        $location .path('#/');
    }

    $scope.addCategory=function(){
        console.log($scope.newCategory);
        var category ={
            name:$scope.newCategory.name
        }
        CategoryFactory.postCategory(category,function(data){
            console.log(data);
            $scope.categories.push(({
                _id: data.insertId,
                name:category.name
            }));
        })


    }
    $scope.editCategory=function(i){
        console.log(i);
        $scope.editCategory=i;
    };

    $scope.doneEditingCategory=function(i){
        $scope.editCategory=-1;
        CategoryFactory.editCategory($scope.categories[i], function(data){});
    };

    $scope.deleteCategory = function(index){

        CategoryFactory.deleteCategory($scope.categories[index]._id, function(data){
            console.log(data);
            $scope.categories.splice(index,1);
        });
    };
    $scope.logout=function(){
        Auth.logout();
        $scope.user=undefined;
        $scope.authenticated=false;
        $scope.displayedModal=1;
    }

    $scope.changeCredentials=function(mode){

        switch(mode){
            case "username":{
                $scope.user.username=$scope.new.username;
                UserFactory.editUser($scope.user,false,function(data){
                    console.log(data);
                });
                console.log($scope.user);
                UserFactory.getUser($scope.user._id,function(data){
                    console.log(data.users[0])
                    $scope.user=data.users[0];
                })
            }break;
            case "name":{
                $scope.user.name=$scope.new.name;
                console.log($scope.user);
                UserFactory.editUser($scope.user,false,function(data){});
                UserFactory.getUser($scope.user._id,function(data){

                    $scope.user=data.users[0];
                })
            }break;
            case "email":{
                $scope.user.email=$scope.new.email;
                UserFactory.editUser($scope.user,false,function(data){});
                UserFactory.getUser($scope.user._id,function(data){
                    $scope.user=data.users[0];
                })
            }break;
            case "password":{
                $scope.user.password=$scope.new.password;
                UserFactory.editUser($scope.user,true,function(data){});
                UserFactory.getUser($scope.user._id,function(data){
                    $scope.user=data.users[0];
                })
            }break;
        }
    }
});

bikeApp.controller('HomeCtrl',function ($scope,Auth,$rootScope,UserFactory,CategoryFactory,ProductFactory,$window,OrderFactory) {
    $scope.selectedTab=0;

    $scope.authenticated = $rootScope.authenticated;
    if ($rootScope.authenticated){
        UserFactory.getUser($rootScope.user._id,function(data){
           $scope.user=data.users[0];
        });
    }

    $scope.tabSwitch=function(category,index){
        $scope.showCategory=category;
        $scope.selectedTab=index;
    }

    ProductFactory.getProduct(function (data){
        $scope.products=data.products;
    })

    $scope.modalShow=function(modal){
        if($scope.displayedModal==modal){
            $scope.displayedModal=1
        }
        else {
            $scope.displayedModal=modal;
        }
    }

    $scope.register=function(valid){
        $scope.displayedModal="login";
        $scope.credentials.level=2;
        if ($scope.credentials.password==$scope.credentials.password2)
            Auth.register($scope.credentials);
    };

    $scope.login=function(){
        Auth.login($scope.credentials, function(logdata){
            console.log(logdata.description);
            $scope.message=logdata.description;
            if(logdata.name!=undefined){
                $scope.user=$rootScope.user
                $scope.authenticated=true;
                $scope.displayedModal=1;
            }
        });

    }

    $scope.logout=function(){
        Auth.logout();
        $scope.user=undefined;
        $scope.authenticated=false;
        $scope.displayedModal=1;
    }

    CategoryFactory.getCategories(function(data){
        console.log(data);
        $scope.categories=data.categories;
        $scope.showCategory=$scope.categories[0];
    })


});

bikeApp.controller('MyAccCtrl',function($scope,Auth,OrderFactory,$rootScope,UserFactory,CategoryFactory, ProductFactory,$location){
    if ($rootScope.authenticated){
        UserFactory.getUser($rootScope.user._id,function(data){
            $scope.user=data.users[0];
        });
    }else{
        $location .path('#/');
    }
    $scope.selected='user';

    OrderFactory.getUserOrders($scope.user._id,function(data){
        console.log($scope.user);
        console.log(data.orders);
        $scope.orders=data.orders
    })

    $scope.logout=function(){
        Auth.logout();
        $scope.user=undefined;
        $scope.authenticated=false;
        $scope.displayedModal=1;
        $location .path('#/');
    }
    $scope.modalShow=function(modal){
        if($scope.displayedModal==modal){
            $scope.displayedModal=1
        }
        else {
            $scope.displayedModal=modal;
        }
    }

    $scope.changeCredentials=function(mode){

        switch(mode){
            case "username":{
                $scope.user.username=$scope.new.username;
                UserFactory.editUser($scope.user,false,function(data){
                    console.log(data);
                });
                console.log($scope.user);
                UserFactory.getUser($scope.user._id,function(data){
                    console.log(data.users[0])
                    $scope.user=data.users[0];
                })
            }break;
            case "name":{
                $scope.user.name=$scope.new.name;
                console.log($scope.user);
                UserFactory.editUser($scope.user,false,function(data){});
                UserFactory.getUser($scope.user._id,function(data){

                    $scope.user=data.users[0];
                })
            }break;
            case "email":{
                $scope.user.email=$scope.new.email;
                UserFactory.editUser($scope.user,false,function(data){});
                UserFactory.getUser($scope.user._id,function(data){
                    $scope.user=data.users[0];
                })
            }break;
            case "password":{
                $scope.user.password=$scope.new.password;
                UserFactory.editUser($scope.user,true,function(data){});
                UserFactory.getUser($scope.user._id,function(data){
                    $scope.user=data.users[0];
                })
            }break;
        }
    }
    $scope.deleteOrder=function(index){
        OrderFactory.deleteOrder($scope.orders[index]._id,function(data){
            $scope.orders.splice(1,index);
        });
    }

    $scope.addToCart=function(product){
        console.log("Sdasdsa");
        if(!$scope.authenticated){
            $scope.displayedModal="login"
        }
        else if($scope.user.level==1){
            $window.alert("Prijavljeni ste kao admin");
        }else
        {
            $scope.user.cart.push(product);
            UserFactory.editUser($scope.user,false,function(data){});
        }
    }
    $scope.removeFromCart=function(item){
        var index= $scope.user.cart.indexOf(item);
        $scope.user.cart.splice(index,1);
        UserFactory.editUser($scope.user,false,function(data){});
    }
    $scope.createNewOrder=function(){
        console.log($scope.user._id);
        var newOrder={
            products:$scope.user.cart,
            user:$scope.user,
            userId:$scope.user._id,
            timestamp: new Date()
        }
        $scope.user.cart.forEach(function(item,index){
            item.onStock--;
            ProductFactory.editProduct(item,function(data){});
        })
        OrderFactory.postOrder(newOrder,function(data){
            $window.alert("Nova narudžba je kreirana");
            $scope.user.cart.splice(0,$scope.user.cart.length);
            UserFactory.editUser($scope.user,false,function(data){});
        })
    }
})

