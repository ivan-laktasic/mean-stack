bikeApp.service('multipartForm',['$http',function($http){

    this.post=function(data,callback){
        console.log(data);
        var formData= new FormData();
        for(var key in data){
            formData.append(key,data[key]);
        }
        $http.post('/api/upload',formData,{
            transformRequest:angular.identity,
            headers:{'Content-Type':undefined}
        }).success(callback)
    }
}])