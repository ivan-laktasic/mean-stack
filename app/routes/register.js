module.exports=function(app, express, db, bcrypt,jwt, secret) {
    var registerRouter = express.Router();

    registerRouter.post('/users', function (req, res){
        bcrypt.hash(req.body.user.password, null, null, function (err, hash) {
            var user = {
                username: req.body.user.username,
                password: hash,
                name: req.body.user.name,
                email: req.body.user.email,
                level:req.body.user.level,
                cart:[]
            };
            db.collection('users').insertOne(user, function (err, data) {
                console.log(data);
                if (!err) {
                    res.json({status: 'ok', insertId: data.insertedId});
                }
                else
                    res.json({status: 'NOT OK'});
            });
        });
    });

    return registerRouter
}
