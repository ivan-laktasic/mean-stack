module.exports=function(app, express, db, bcrypt, jwt,secret){

    var authRouter = express.Router();
    authRouter.post('/', function(req,res){
        db.collection('users').find({
            username:req.body.credentials.username
        }).toArray(function(err, rows){
            if (rows.length==0)  res.json({ status: 'not ok', description:'Username doesnt exist' }); else {
                var validPass = bcrypt.compareSync(req.body.credentials.password, rows[0].password);
                if (!err) {
                    if (rows.length > 0 && validPass) {
                        var token = jwt.sign({
                            username:rows[0].username,
                            password:rows[0].password,
                            _id:rows[0]._id,
                            level:rows[0].level
                        },secret, {
                            expiresIn:1440
                        });
                        res.json({
                            status: 'ok',
                            user: {
                                username: rows[0].username,
                                email: rows[0].email,
                                name: rows[0].name,
                                level: rows[0].level,
                                cart: rows[0].cart,
                                token:token,
                                password:rows[0].password
                            }
                        });
                    } else {
                        res.json({status: 'not ok', description: 'Wrong password'});
                    }
                }
                else
                    res.json({status: 'not ok', description: 'Database error'});
            }
        });
    });
    return authRouter;
};