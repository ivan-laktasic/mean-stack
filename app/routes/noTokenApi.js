module.exports=function(app, express, db) {

    var noTokenApiRouter = express.Router();
    var ObjectId = require('mongodb').ObjectId;

    noTokenApiRouter.route('/products').get(function (req, res) {
        db.collection('products').find({}).toArray(function (err, rows) {
            if (!err) {
                res.json({status: 'OK', products: rows});
            }
            else
                res.json({status: 'NOT OK'});
        });
    });

    noTokenApiRouter.route('/categories').get(function (req, res) {
        db.collection('categories').find({}).toArray(function (err, rows) {
            if (!err) {
                res.json({status: 'OK', categories: rows});
            }
            else
                res.json({status: 'NOT OK'});
        })
    });

    noTokenApiRouter.route('/categories/:id').get(function(req,res){
        console.log(req.params.id);
        db.collection('categories').find({"_id": ObjectId(req.params.id)}).toArray(function (err, rows) {
            if (!err) {
                res.json({status: 'OK', categories: rows});
            }
            else
                res.json({status: 'NOT OK'});
        })
    });

    return noTokenApiRouter
}