module.exports=function(app, express, db, bcrypt,jwt,secret,multer) {

    var apiRouter = express.Router();
    var ObjectId = require('mongodb').ObjectId;
    apiRouter.use(function(req, res, next){

        var token = req.body.token || req.params.token || req.headers['x-access-token'];
        if (token){
            jwt.verify(token, secret, function (err, decoded){
                if (err){
                    return res.status(403).send({
                        success:false,
                        message:'Wrong token'
                    });
                } else {
                    req.decoded=decoded;
                    next();
                }
            });
        } else {
            return res.status(403).send({
                success:false,
                message:'No token'
            });
        }
    });

    apiRouter.get('/', function(req, res) {
        res.json({ message: 'api' });
    });

    apiRouter.post('/users', function (req, res){
        bcrypt.hash(req.body.user.password, null, null, function (err, hash) {
            var user = {
                username: req.body.user.username,
                password: hash,
                name: req.body.user.name,
                email: req.body.user.email,
                level:req.body.user.level,
                cart:[]
            };
            db.collection('users').insertOne(user, function (err, data) {
                console.log(data);
                if (!err) {
                    res.json({status: 'ok', insertId: data.insertedId});
                }
                else
                    res.json({status: 'NOT OK'});
            });
        });
    });
    apiRouter.route('/users').get(function (req, res) {
        db.collection('users').find({}).toArray(function (err, rows) {
            if (!err) {
                res.json({status: 'OK', users: rows});
            }
            else
                res.json({status: 'NOT OK'});
            })
    }).put(function (req, res) {
        if(req.body.passChange){
            bcrypt.hash(req.body.user.password, null, null, function (err, hash) {
                var user = {
                    username: req.body.user.username,
                    password: hash,
                    name: req.body.user.name,
                    email: req.body.user.email,
                    cart: req.body.user.cart,
                    level: req.body.user.level
                };
            });
        }else{
            var user= {
                username: req.body.user.username,
                password:req.body.user.password,
                name:req.body.user.name,
                email:req.body.user.email,
                cart:req.body.user.cart,
                level:req.body.user.level
            };
        }
        console.log(user);
        db.collection('users').updateOne(
            {_id: ObjectId(req.body.user._id)},
            {$set: user},
            function (err, data) {
                if (!err) {
                    res.json({status: 'OK', changedRows: data.nModified});
                }
                else
                    res.json({status: 'NOT OK'});
            }
        );
    });
    apiRouter.route('/users/:id').delete(function (req, res) {
        db.collection('users').removeOne({
            _id: ObjectId(req.params.id)
        }, function (err, data) {
            if (!err) {
                res.json({status: 'OK', affectedRows: data.nModified});
            }
            else
                res.json({status: 'NOT OK'});
        });
    }).get(function(req,res){
        db.collection('users').find({"_id":ObjectId(req.params.id)}).toArray(function (err, rows) {
            if (!err) {
                res.json({status: 'OK', users: rows});
            }
            else
                res.json({status: 'NOT OK'});
        })
    });
/******************************************************************************/

    apiRouter.post('/products', function (req, res) {
            console.log(req.body.product.category);
            var product = {
                name: req.body.product.name,
                picUrl: req.body.product.picUrl,
                description: req.body.product.description,
                price: req.body.product.price,
                categoryName:req.body.product.categoryName,
                categoryId:req.body.product.categoryId,
                onStock:req.body.product.onStock
            }

            db.collection('products').insertOne(product, function (err, data) {
                if (!err) {
                    res.json({status: 'ok', insertId: data.insertedId});
                }
                else
                    res.json({status: 'NOT OK'});
            });
        });
    apiRouter.route('/products').put(function (req, res) {
        var product = {
            name: req.body.product.name,
            picUrl: req.body.product.picUrl,
            description: req.body.product.description,
            price: req.body.product.price,
            categoryName:req.body.product.categoryName,
            categoryId:req.body.product.categoryId,
            onStock:req.body.product.onStock
        }
        console.log(req.body);
        db.collection('products').updateOne(
            {_id: ObjectId(req.body.product._id)},
            {$set: product},
            function (err, data) {
                if (!err) {
                    res.json({status: 'OK', changedRows: data.nModified});
                }
                else
                    res.json({status: 'NOT OK'});
                console.log(err);
            }
        );
    });
    apiRouter.route('/products/:id').delete(function (req, res) {
        db.collection('products').removeOne({
            _id: ObjectId(req.params.id)
        }, function (err, data) {
            if (!err) {
                res.json({status: 'OK', affectedRows: data.nModified});
            }
            else
                res.json({status: 'NOT OK'});
        });
    });
/*************************************************************************/
    apiRouter.post('/orders', function (req, res) {

        var order = {
            products: req.body.order.products,
            user: req.body.order.user,
            userId:req.body.order.userId,
            timestamp: req.body.order.timestamp
        }

        db.collection('orders').insertOne(order, function (err, data) {
            console.log(data);
            if (!err) {
                res.json({status: 'ok', insertId: data.insertedId});
            }
            else
                res.json({status: 'NOT OK'});
        });
    });
    apiRouter.route('/orders').get(function (req, res) {
        db.collection('orders').find({}).toArray(function (err, rows) {
            if (!err) {
                res.json({status: 'OK', orders: rows});
            }
            else
                res.json({status: 'NOT OK'});
        });
    }).put(function (req, res) {
        var order = {
            products: req.body.order.products,
            user: req.body.order.user,
            userId:req.body.order.userId,
            timestamp: req.body.order.timestamp
        }
        db.collection('orders').updateOne(
            {_id: ObjectId(req.body.order._id)},
            {$set: order},
            function (err, data) {
                if (!err) {
                    res.json({status: 'OK', changedRows: data.nModified});
                }
                else
                    res.json({status: 'NOT OK'});
            }
        );
    });
    apiRouter.route('/orders/:id').delete(function (req, res) {
        db.collection('orders').removeOne({
            _id: req.params.id
        }, function (err, data) {
            if (!err) {
                res.json({status: 'OK', affectedRows: data.nModified});
            }
            else
                res.json({status: 'NOT OK'});

            });
    }).get(function(req,res){
            console.log(req.params);
            db.collection('orders').find({
                "userId":req.params.id
            }).toArray(function (err, rows) {
                if (!err) {
                    res.json({status: 'OK', orders: rows});
                }
                else
                    res.json({status: 'NOT OK'});
            });
        });

    /********************************************************************************************/

    apiRouter.post('/categories', function (req, res) {

        var category = {
            name: req.body.category.name,
        }

        db.collection('categories').insertOne(category, function (err, data) {
            console.log(data);
            if (!err) {
                res.json({status: 'ok', insertId: data.insertedId});
            }
            else
                res.json({status: 'NOT OK'});
        });
    });
    apiRouter.route('/categories').put(function (req, res) {
        var category = {
            name: req.body.category.name,
        }

        db.collection('categories').updateOne(
            {_id: ObjectId(req.body.category._id)},
            {$set: category},
            function (err, data) {
                if (!err) {
                    res.json({status: 'OK', changedRows: data.nModified});
                }
                else
                    res.json({status: 'NOT OK'});
            }
        );
    });
    apiRouter.route('/categories/:id').delete(function (req, res) {
        db.collection('categories').removeOne({
            _id: ObjectId(req.params.id)
        }, function (err, data) {
            if (!err) {
                res.json({status: 'OK', affectedRows: data.nModified});
            }
            else
                res.json({status: 'NOT OK'});
        });
    })


    apiRouter.get('/me', function (req, res){
        res.send(req.decoded);
    });

    apiRouter.post('/upload',multer ,function(req,res){
        console.log("********");
        console.log(req);
        console.log(req.file);
        res.json({success: true,url:req.file.filename});
    })

    return apiRouter;
    }


